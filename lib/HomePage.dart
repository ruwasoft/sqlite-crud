import 'package:flutter/material.dart';
import 'package:sqlite_ex/db_helper.dart';

import 'employee.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  Future<List<Employee>>? employees;
  TextEditingController controller = TextEditingController();
  late String name;
  late int curUserID;

  final formKey = GlobalKey<FormState>();
  var dbHelper;
  late bool isUpdating;


  @override
  void initState() {
    super.initState();

    dbHelper = DBHelper();
    isUpdating = false;

    refreshList();

  }

  refreshList()
  {
    employees = dbHelper.getEmployees();

    setState(() {});
  }


  clearName(){
    controller.text="";
  }


  validate()
  {

    if(formKey.currentState!.validate())
      {

        formKey.currentState!.save();

        if(isUpdating)
          {
            Employee employee = Employee(curUserID, name);
            dbHelper.update(employee);

            setState(() {
              isUpdating = false;
            });

            clearName();

          }
        else
          {

            Employee employee = Employee(null, name);
            dbHelper.save(employee);

            setState(() {
            });

          }

      }

  }

  form()
  {
    return Form(
      key: formKey,
        child: Padding(
          padding: EdgeInsets.all(15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            verticalDirection: VerticalDirection.down,

            children:
            [
              TextFormField(
                controller: controller,
                keyboardType: TextInputType.text,
                decoration: const InputDecoration(labelText: 'Name'),
                validator: (val)=> val!.isEmpty? "Enter Name":null,
                onSaved: (val) => name = val!,
              ),


              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children:
                [

                  TextButton(
                      onPressed: (){
                        validate();

                        refreshList();

                      },
                      child: Text(isUpdating? 'Update': 'Add'),
                  ),


                  TextButton(
                    onPressed: (){
                      setState(() {
                        isUpdating=false;
                      });

                      clearName();
                    },
                    child: const Text('Cancel'),
                  )


                ],
              ),


            ],

          ),
        )
    );
  }

  dataTable(List<Employee>? employees)
  {
      return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: DataTable(
          columns:
          [
            DataColumn(
                label: Text("Name")
            ),

            DataColumn(
                label: Text("Delete")
            ),

          ],
          rows: employees!.map((employee) => DataRow(
              cells: [
                DataCell(
                  Text(employee.name),
                  onTap: (){

                    setState(() {

                      isUpdating =true;

                      curUserID = employee.id!;

                      controller.text = employee.name;


                    });

                  }
                ),

                DataCell(
                  IconButton(
                    icon: const Icon(Icons.delete),
                    onPressed: (){
                      dbHelper.delete(employee.id);
                      refreshList();
                    },
                  ),
                )

              ]
          )).toList(),
        ),
      );
  }

  list()
  {
    return Expanded(
        child: FutureBuilder(
          future: employees,
          builder: (context,snapshot)
          {
            if(snapshot.hasData)
              {
                return dataTable(snapshot.data);
              }

            if(null==snapshot.data||snapshot.data!.isEmpty)
              {
                return const Text("No data found");
              }

            else
              {
                return const Text("No data found");
              }


          },
        )
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: const Text("SQLite CRUD",
        style: TextStyle(
          color: Colors.white
        ),
        ),
        backgroundColor: Colors.indigo,
      ),

      body: Container(
          child: Column(
            children: [

              form(),
              list(),

            ],
          ),
      ),

    );
  }
}
